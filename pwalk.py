r'''
`pwalk` is a python module to generate and visualize the random movement of 
open polymer chain. You might want to check out its 
`GitLab page <https://gitlab.gwdg.de/ben.reinhold/SciComPrac>`_.
'''

import numpy as np
from mayavi import mlab
import moviepy.editor as mpy
import itertools


def hit_and_run(A,b,x_0, p=10):
    r'''Generator that generates random points in a bounded convex polytope.
    
    Generator that uses the :ref:`hit and run algorithm <hnr>` to generate 
    random points in the convex polytope defined 
    by the system of linear inequalities :math:`Ax \leq  b`.
    
    Parameters
    ----------
    A : (m,n) ndarray
        Matrix used to define the polytope
    b : (n,) ndarray
        Vector used to define the polytope
    x_0 : (n,) ndarray
        Starting point for hit and run algorithm
    p : int, optional
        Only yields every `p` th point. Defaults to 10.
        
    Yields
    ------
    array_like
       Random point in polytope
        
    Examples
    --------
    >>> import numpy
    >>> A = numpy.array([[1, 0], [0, 1], [-1, 0], [0, -1]])
    >>> b = numpy.array([1, 2, 1, 2])
    >>> next(hit_and_run(A, b, numpy.zeros(2)))
    array([0.32924054, 0.78864707])  # random

    Notes
    ------
    - A *ValueError* may be raised if the polytope is not bounded
    - The starting point x_0 must be an element of the polytope
    '''
    m,n = A.shape
    
    # Check that x_0 is in polytope
    assert all(A.dot(x_0) <= b), 'startpoint not in polytope'
    assert type(p) == int, 'stepsize has to be an integer'
    assert p >= 1, 'stepsize has to be positive' 
    
    c = 0
    while True:
        
        c += 1

        # generate normed vector with random direction
        d = np.random.random(n)*2-1
        d = d*(1/sum(d**2)**0.5)

        bx= b-A.dot(x_0)
        Ad = A.dot(d)

        # compute componentwise boundaries for t, s.t. tAd <= bx
        upbound = []
        lowbound = []
        
        for i in range(m):
            
            Adi=Ad[i]
            bxi=bx[i]
            
            if Adi==0:
                break
            elif Adi>0:
                upbound.append(bxi/Adi)
            else:
                lowbound.append(bxi/Adi)
        
        # take the strongest bounds
        upbound = min(upbound)
        lowbound = max(lowbound)
        
        t = np.random.random()*(upbound-lowbound)+lowbound
        x_1 = x_0+t*d
        x_0 = x_1
        
        if c == p:
            c = 0
            yield x_0


def rtorus(n):
    r'''Generates a random, uniformly distributed point on the n-torus.   

    Parameters
    ----------
    n : int
        dimension of torus
        
    Returns
    -------
    (n,) ndarray
       Random point on the `n`-torus.
        
    Examples
    --------
    >>> rtorus(4)
    array([1.97154854, 0.35630964, 3.89600292, 5.0084903])  # random
    '''
    
    return np.random.rand(n)*2*np.pi


def unconfined(r):
    r'''
    Generates matrix and vector to define the moment polytope for an 
    unconfined walk.
    
    Generates a matrix `A` and a vector `b` such that 
    :math:`Ax \leq b` is the system of linear inequalities 
    :math:`-r_i \leq x_i \leq r_i`.
    
    Parameters
    ----------
    r : (n,) ndarray
        :math:`(r_1,...,r_n)`
        
    Returns
    -------
    (2n,n) ndarray
       Matrix to define polytope
    (2n,) ndarray
       Vector to define polytope
        
    Examples
    --------
    >>> import numpy
    >>> unconfined(numpy.ones(2))
    (array([[ 1.,  0.],
            [ 0.,  1.],
            [-1., -0.],
            [-0., -1.]]), array([1., 1., 1., 1.]))
    '''
    
    n = r.shape[0]
    return np.concatenate((np.eye(n),-1*np.eye(n))), np.concatenate((r,r))


def slabconfined(n,h):
    r'''Generates matrix and vector to constrain the moment 
    polytope for a :ref:`slab-confined <confinements>` walk.
    
    Generates a matrix `A` and a vector `b` such that :math:`Ax \leq b` is the 
    system of linear inequalities :math:`-h \leq \sum_{k=i}^j x_k \leq h` for 
    :math:`1\leq i\leq j\leq n` and height `h`.

    
    Parameters
    ----------
    n : int
        Dimension of polytope
    h : float
        Slabheight
        
    Returns
    -------
    (n*(n+1),n) ndarray
       Matrix to define polytope
    (n*(n+1),) ndarray
       Vector to define polytope
        
    Examples
    --------
    >>> slabconfined(2, 3)
    (array([[ 1.,  0.],
            [ 1.,  1.],
            [ 0.,  1.],
            [-1., -0.],
            [-1., -1.],
            [-0., -1.]]), array([3., 3., 3., 3., 3., 3.]))
    '''
    
    A = np.stack([np.concatenate((np.zeros(i),np.ones(j-i+1),np.zeros(n-1-j)))for i in range(n) for j in range(i,n)])
    return np.concatenate((A,-A)), np.ones(n*(n+1))*h


def hsconfined(n):
    r'''Generates matrix and vector to constrain the moment 
    polytope for a :ref:`half-space confined <confinements>` walk.
    
    Generates a matrix `A` and a vector `b` such that :math:`Ax \leq b` is the 
    system of linear inequalities :math:`0 \leq \sum_{k=1}^i x_k` 
    for :math:`1\leq i\leq n`.

    Parameters
    ----------
    n : int
        Dimension of polytope
        
    Returns
    -------
    (n,n) ndarray
       Matrix to define polytope
    (n,) ndarray
       Vector to define polytope
        
    Examples
    --------
    >>> hsconfined(4)
    (array([[-1., -0., -0., -0.],
            [-1., -1., -0., -0.],
            [-1., -1., -1., -0.],
            [-1., -1., -1., -1.]]), array([0., 0., 0., 0.]))
    '''    
    return -1*np.tri(n), np.zeros(n)


def spherify(z,phi,r):
    r'''Converts a parametrization of the 2-sphere of radius `r` in terms of 
    height `z` and azimuthal angle `phi` into cartesian coordinates.

    Parameters
    ----------
    z : float
        Height coordinate 
    phi : float
        Angle coordinate 
    r : float
        Radius of sphere
        
    Returns
    -------
    tuple of floats
       Cartesian coordinates
        
    Examples
    --------
    >>> spherify(0, 0, 1)
    (1.0, 0.0, 0)
    '''
    return np.cos(phi)*(r**2-z**2)**(0.5), np.sin(phi)*(r**2-z**2)**(0.5), z


def tsmcmc(A,b,x_0,beta=0.5):
    r'''toric symplectic Markov Chain Monte Carlo Algorithm, see 
    :cite:`Cantarella-Shonkwiler` for details.
    
    Generates random action and angle coordinates with action coordintates 
    in the polytope defined by :math:`Ax \leq b`. In each iteration, either the
    action or the angle coordinates gets updated.
     
    Parameters
    ----------
    A : (m,n) ndarray
        Matrix to define polytope
    b : (m,) ndarray
        Vector to define polytope
    x_0 : (n,) ndarray
        Startpoint for Hit and Run algorithm
    beta : float, optional
        Propability in each iteration to update action coordinates. Defaults
        to 0.5.
        
    Yields
    ------
    (n,) ndarray
       action coordinates
    (n,) ndarray
       angle coordinates
        
    Examples
    --------
    >>> import numpy
    >>> A, b = unconfined(numpy.ones(3))
    >>> [next(tsmcmc(A,b,np.zeros(3))) for i in range(2)]
    [(array([0., 0., 0.]), array([1.93090866, 3.61597696, 5.69873831])),
     (array([-0.28832145, -0.67717484, -0.37535512]),
      array([1.74749102, 5.88019383, 2.45307094]))]  # random

    '''
    
    _,n=A.shape
    
    HR = hit_and_run(A,b,x_0)
    
    ac = x_0
    an = rtorus(n)
    
    while 1:
        if np.random.rand() <= beta:
            ac = next(HR)
        else:
            an = rtorus(n)
        
        yield ac, an
        

def polygif(A, b, x_0, r, beta=0.5, t=5, fps=3, size=(800,800), distance=0, center=True, name='RandomWalk'):
    r'''Creates a gif of the random walk of an open polygon arm.
    
    Animates the random walk of an open polygon arm in 3-space by sampling
    the torus and the polytope defined by the system of linear inequalities 
    :math:`Ax\leq b`.
    
    Parameters
    ----------
    A : (m,n) ndarray
        Matrix to define the moment polytope
    b : (m,) ndarray
        Vector to define the moment polytope where the first n entries are the tube lengths
    x_0 : (n,) ndarray
          Starting point for hit and run algorithm, has to satisfy 
          the inequality :math:`Ax\leq b`.
    r : (n,) ndarray
        Lengths of the edges of the polygon arm.
    beta : float
           Parameter for :any:`tsmcmc`. 
           Defaults to 0.5
    t : int, optional
        Duration of the gif. Defaults to 5.
    fps : int, optional
          Frames per second used in the gif. Defaults to 3.
    size : tuple of int, optional
           Size of the gif screen in pixel. Defaults to (800,800)
    distance : float, optional
               Distance between camera and focal point.
    center : bool, optional
             Sets the focal point of the animation to the be the center of the 
             polygon arm if `True`, fixes one endpoint to (0,0,0) otherwise.
             Defaults to `True`.
    name : str, optional
           File name for the gif (without filename extension). Defaults to
           'RandomWalk'
           
        
    Returns    
    -------
    str
       returns "Done" if animation was succesfully saved to `name.gif`.
        
    Notes
    -----
    - You can use the :any:`unconfined`, :any:`slabconfined` and 
      :any:`hsconfined` to generate `A` and `b` for an unconfined,
      a slab confined and a half-space confined walk, respectively. See 
      their documentation for details. You can find examples 
      :doc:`here <examples>`. 
      
      
    
    Examples
    --------
    >>> import numpy
    >>> r = numpy.ones(8)
    >>> A, b = unconfined(r)
    >>> polygif(A, b, np.zeros(8), r, t=10, fps=1, name="file_name")
    "Done"
    
    >>> import numpy
    >>> r = numpy.random.random(17)
    >>> A1, b1 = unconfined(r)
    >>> A2, b2 = hsconfined(17)
    >>> A = numpy.concatenate((A1,A2))
    >>> b = numpy.concatenate((b1,b2))
    >>> polygif(A, b, np.zeros(17), r, size=(600,400), dist=20, center=False)
    "Done"
    
    '''
    
    n = A.shape[1]
    N = t*fps + 1 
    
    # Rescales for convenience
    b = (n/sum(r))*b
    r = (n/sum(r))*r
    
    # If distance is not supplied, set d to a default value
    if distance == 0:
        distance = 15+2*n**0.5
    
    # Generates random points on a sphere
    Alg = tsmcmc(A, b, x_0, beta=beta) 
    data = np.array([next(Alg) for _ in range(N)])
    zdata, phidata = data[:,0,:], data[:,1,:]
    x,y,z = spherify(zdata, phidata, r)

    # Computes the Uses the random points of the sphere to create coordinates of the knots
    Summation_Mat = np.tri(n)
    for var in [x,y,z]:
        for i in range(N):
            var[i] = Summation_Mat.dot(var[i])
    
    # Matrix with the x,y,z coordinates for each frame in the rows 
    x = np.concatenate((np.zeros((N,1)), x), axis=1)
    y = np.concatenate((np.zeros((N,1)), y), axis=1)
    z = np.concatenate((np.zeros((N,1)), z), axis=1)
    
    # Avoids rendering window
    mlab.options.offscreen = True
    
    fig = mlab.figure(size=size, bgcolor=(0.1,0.1,0.1))

    # Function to create the frames
    n_gen = itertools.count()
    def make_frame(t):
        mlab.clf()
        
        # Get the x,y,z coordinates for the next frame
        c = next(n_gen)
        nx = x[c]
        ny = y[c]
        nz = z[c]

        # Comuputes the focal point
        if not center:
            c_point=(0,0,0)
        else:
            cx = sum(nx)/len(nx)
            cy = sum(ny)/len(ny)
            cz = sum(nz)/len(nz)
            c_point = (cx, cy, cz)
        
        # Plot edges
        mlab.plot3d(nx, ny, nz, line_width=0, figure=fig, color=(1,1,1), tube_radius=0.02)
        
        # Plot vertices
        mlab.points3d(nx, ny, nz, np.ones_like(nx), scale_factor=0.2, figure=fig, color=(0.3,0.6,1))
        
        # Setting focalpoint and distance
        mlab.view(focalpoint=c_point, distance=distance, elevation=90)
        
        return mlab.screenshot(antialiased=True)
    
    # Animate and save as gif
    animation = mpy.VideoClip(make_frame, duration=t)
    animation.write_gif("{}.gif".format(name), fps=fps)
    
    mlab.close()
    
    return 'Done'