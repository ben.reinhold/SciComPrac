The Symplectic Geometry of Random Walks
=======================================

A random walk in three dimensions is the repeated random choice of unit vectors in :math:`\mathbb S^{2}\subset\mathbb R^3`. With this, one can simulate for example the random movement of an open polymer chain:

.. image:: Animations/rwalk.*

The configuration space of such an open polygon arm with :math:`n` edges of lengths :math:`\vec r=(r_1,\dotsc,r_n)\in \R_+^n` is 

.. math:: \Arm_3(\vec r;n)= \Sph^2_{r_1}\times\dotsc\times \Sph^2_{r_n},

when we identify configurations that differ only by a translation.

Symplectic Manifolds
--------------------

A *symplectic manifold* :math:`(M,\omega)` is a smooth manifold :math:`M` together with a *symplectic form* :math:`\omega`, that is a closed nondegenerate 2-form 
:math:`\omega \in \Omega^2(M)`. 

**Examples:**

1. Let :math:`M=\Sph^2_r` be the unit 2-sphere with radius :math:`r\in\R_{+}`. The usual riemannian volume form :math:`\omega = r \sin(\vartheta)\text{d}\vartheta\wedge \text{d} \varphi` on :math:`M` is symplectic, where :math:`(\vartheta,\varphi) \in (0,\pi)\times (0,2\pi)` are spherical coordinates on `M`.

2. Let :math:`(M_i,\omega_i)` be symplectic manifolds for :math:`i=1,\dotsc, n`. :math:`\omega = \text{pr}_1^*\omega_1+\dotsc+ \text{pr}_n^*\omega_n` is then a symplectic form on :math:`M=M_1\times\dotsc\times M_n`, where :math:`\text{pr}_i` denotes the projection :math:`M\to M_i` for :math:`i=1,\dotsc,n`. This way, :math:`\Arm_3(n,\vec r)` becomes a symplectic manifold for each :math:`\vec r\in \R_+^n`.

A diffeomorphism :math:`f\colon M_1\to M_2` between symplectic manifolds :math:`(M_1,\omega_1)` and :math:`(M_2,\omega_2)` is called a *symplectomorphism* if :math:`f^*\omega_2=\omega_1`.


The Symplectic Measure
----------------------

Let :math:`(M,\omega)` be a :math:`2n`-dimensional symplectic manifold. As :math:`\omega` is nondegenerate, :math:`\omega^n` has to be nonvanishing and :math:`\frac{\omega^n}{n!}` is then a volume form on :math:`M`. In particular, :math:`M` is orientable. The Borel measure on :math:`M` induced by :math:`\frac{\omega^n}{n!}` is called the *symplectic measure* and will be denoted by :math:`m_\omega`. Explicitly, we have for :math:`U\subset M` open:

.. math:: m_\omega(U)=\int_U \frac{\omega^n}{n!}

If :math:`M` is compact, it has finite volume and we can normalize :math:`m_\omega` to obtain a propability measure on :math:`M`.

It's immediate that a symplectomorphism between symplectic manifolds of the same dimension preserves the (normalized) symplectic measure.

**Examples:**

1. Note that the symplectic form :math:`r\sin(\vartheta)\dif \vartheta \wedge\dif\varphi` on :math:`\Sph^2_r` becomes 

   .. math:: \omega = \dif \varphi\wedge\dif z

   in cylindrical coordinates :math:`(z,\varphi)=(r\cos(\vartheta),\varphi)`. This way, :math:`\Sph^2_r` minus its poles is an open full measure subset of `\Sph^2_r` that is symplectomorph to :math:`\Sph^1\times (-r,r)` with its usual symplectic form :math:`\dif\varphi\wedge\dif z`. Sampling :math:`\Sph^2_r` with respect to the (normalized) symplectic measure, which is the usual two dimensional volume on :math:`\Sph^2_r` (up to a constant), then amounts to sampling :math:`(-r,r)` and :math:`\Sph^1` independently with respect to the Lebesgue measure on :math:`(-r,r)` and the uniform measure on :math:`\Sph^1`.

2. Let :math:`(M_i,\omega_i)` be compact symplectic manifolds for :math:`i=1,\dotsc, n`. Its not hard to see that the symplectic measure :math:`m_\omega` on :math:`M=M_1\times\dotsc\times M_n` for :math:`\omega = \text{pr}_1^*\omega_1+\dotsc+ \text{pr}_n^*\omega_n` is then the product measure of the measures :math:`m_{\omega_1},\dotsc, m_{\omega_n}`.

Applying this componentwise to :math:`\Arm_3(n,\vec r)`, we get a symplectomorphism between an open full measure subset of :math:`\Arm_3(n,\vec r)` and 

.. math:: (-r_1,r_1)\times \Sph^1\times\dotsc\times (-r_n,r_n)\times \Sph^1\cong \prod_{i=1}^n (-          r_i,r_i)\times \T^n

with its standard symplectic form :math:`\sum_{i=1}^{n}\dif \varphi_i\wedge \dif z_i`, where :math:`(z_1,\dotsc,z_n)` and :math:`(\varphi_1,\dotsc,\varphi_n)` are the usual coordinates on :math:`\prod_{i=1}^n (-r_i,r_i)\subset \R^n` and :math:`\T^n`, respectively. As the induced voume form is then given by :math:`\varphi_1\dotsc\wedge\dif\varphi_n\wedge \dif z_1\wedge\dotsc\wedge \dif z_n`, sampling :math:`\Arm_3(n,\vec r)` with respect to the symplectic measure then amounts to sampling the interior of :math:`P:=\prod_{i=1}^n[-r_i,r_i]` and :math:`\T^n` independently. 

The advantage of this symplectic geometric approach is that constraints on the movement of the polygon arm can be added in form of restrictions of :math:`P` to a subpolytope:

.. _confinements:

Slab and Half-space confinements
--------------------------------
A **slab-confinement** means that the polygon arm is only allowed to move in a slab of thickness :math:`h\in \R_+`. This amounts to restricting :math:`P` to the subpolytope defined by the inequalities 

.. math:: -h\leq\sum_{k=i}^j z_i\leq h 

for :math:`1\leq i\leq j \leq n`.

A **half-space confinement** means that if we fix one of the endpoints of the open polygon arm to the origin, the polygon arm is only allowed to move in the halfspace defined by :math:`z\geq 0`. As we already identify polygon arms that only differ by a translation, fixing an endpoint to the origin is no constraint on the configuration space :math:`\Arm_3(\vec r;n)`. Constraining the movement to the upper half-space on the other hand amounts to restricting :math:`P` to the subpolytope defined by the inequalities 

.. math:: \sum_{k=1}^i z_k \geq 0`` 

for :math:`i=1,\dotsc,n`.

.. _hnr: 

Hit and Run
-----------
We broke the (confined) random walk of an open polygon arm down to sampling :math:`\T^n` and a bounded convex polytope :math:`P\subset\R^n` that is of the form :math:`P= \{ x\mid Ax\leq b\}` for an :math:`(m\times n)`-matrix :math:`A` and a :math:`b\in \R^m`.
While sampling :math:`\T^n` amounts to choosing independently :math:`n` points uniformly from :math:`[0,2\pi)`, sampling a :math:`P` is more complicated. For this, we will use the hit and run algorithm: 

From a starting point :math:`x_0\in P`, we choose a direction :math:`d\in\Sph^{n-1}` at random. This defines a straight line :math:`x_0+t\cdot d` through :math:`x_0`, parameterized by :math:`t\in \mathbb R`. As :math:`P` is convex and bounded, exactly one line segment is contained in :math:`P` from which we then take a uniformly distributed point. We can then repeat this with our new point as startpoint. In each step, finding the endpoints of the line segment after the choice of a :math:`d` amounts to solving the inequality

.. math:: t\cdot Ad \leq b-Ax_0

componentwise. The following animation visualizes the algorithm for :math:`n=2` and :math:`P=[-1,1]^2`:

.. image:: ./Animations/Hit_and_Run.*

Following :cite:`Cantarella-Shonkwiler`, we only take every 10th point generated this way to avoid correlation.

While this fully describes the algorithm used here to generate random walks, this algorithm can be used to sample a larger class of symplectic manifolds. For this, we will take a look at *symplectic toric manifolds* and *action-angle coordinates*.
 
Hamiltonian Lie group actions
-----------------------------

Let :math:`(M,\omega)` be a symplectic manifold. By nondegeneracy of :math:`\omega`, :math:`M` has to be even-dimensional and :math:`v_p\mapsto \omega_p(v_p, \,\cdot\,)` defines an isomorphism of vector bundles :math:`TM\to T^*M`. This induces an isorphism :math:`\mathfrak X(M)\to \Omega^1(M)`, :math:`X\mapsto i_X\omega`. For :math:`f\in C^\infty(M)`, there is then a unique vector field :math:`X_f\in \mathfrak X(M)` satisfying :math:`i_{X_f}\omega=df`. We call :math:`X_f` the *hamiltonian vector field of* :math:`f`. 

Let :math:`G` be a Lie group and :math:`\Phi\colon G\times M\to M` a smooth action of :math:`G` on :math:`M`. Let :math:`\mathfrak g=T_eG` be the Lie algebra of :math:`G`. For :math:`x\in\mathfrak g`, we let :math:`x_M\in\mathfrak X(M)` denote the vector field defined by 
:math:`x_M(p)=T_{(e,p)}\Phi(x,0_p)`
for :math:`p\in M`. These vector fields are called the *infinitesimal generators* of the Lie group action.
If :math:`\Phi_g` is a symplectomorphism for all :math:`g\in G`, the action :math:`\Phi` is said to be *symplectic*.

**Examples:**

1. For :math:`M=\Sph^2_r` as in the previous exampleshe unit 1-sphere :math:`\Sph^1` acts on :math:`\Sph^2_r` by rotation around the :math:`z`-axis. As the symplectic form on :math:`\Sph^2_r` is invariant under rotations, this action is symplectic. Identifying the Lie algebra of :math:`\Sph^2` with :math:`\R` and leting :math:`x=1\in\R`, the infinitesimal generator of this action is given by :math:`x_M= r\sin(\vartheta)\partial/\partial \varphi` on the domain of :math:`(\vartheta,\varphi)` and vanishes on the poles as these are the fixed points of this action.
2. Let :math:`G_i` be a Lie group acting on a symplectic manifold :math:`(M_i,\omega_i)` in a symplectic way for each :math:`i=1,\dotsc,n`. The componentwise action of :math:`G=G_1\times\dotsc\times G_n` on :math:`M=M_1\times\dotsc\times M_n` is then symplectic, where :math:`M` carries the symplectic structure defined above. This way, we get a symplectic action of :math:`\T^n=\Sph^1\times\dotsc\times \Sph^1` on :math:`\Arm_3(n,\vec r)`.

Assume now that the action of :math:`G` on :math:`M` is symplectic and, for simplicity, that :math:`G` is abelian (e.g. `G=\T^n`). A map :math:`\mu \colon M\to \mathfrak g^*` is called a *moment map* if 
:math:`i_{x_M}\omega = d\mu^x`
for all :math:`x\in \mathfrak g`, where :math:`\mu^x\in C^\infty(M)` is defined by :math:`\mu^x(p)=\mu(p)(x)` for :math:`p\in M`. We can write this equivalently as :math:`X_{\mu^x}= x_M` for all :math:`x\in\mathfrak g`. An action of :math:`G` on :math:`M` is called *hamiltonian*, if it's symplectic and admits a :math:`G`-invariant moment map :math:`\mu\colon M\to \mathfrak g^*`. 

**Examples:**

1. Consider the action of :math:`\Sph^1` on :math:`\Sph^2_r` as above. Identifying :math:`\R \cong \R^{\ast}`, :math:`\mu\colon \Sph^2_r\to \R`, :math:`(x,y,z)\mapsto z` is a :math:`\Sph^1`-invariant moment map with image :math:`[-r,r]\subset \R`.
2. Let :math:`G_i` be an abelian Lie group acting on a symplectic manifold :math:`(M_i,\omega_i)` in a hamiltonian way with :math:`G_i`-invariant moment map :math:`\mu_i` for each :math:`i=1,\dotsc,n`. Denoting the Lie algebra of :math:`G_i` by :math:`\mathfrak g_i` for each :math:`i`, the Lie algebra of the product Lie group :math:`G=G_1\times\dotsc\times G_n` decomposes as :math:`\mathfrak g= \g_1\oplus\dotsc\oplus \g_n`. :math:`\mu=(\pr_1^*\mu_1,\dotsc,\pr_n^\ast\mu_n)` is then a :math:`G`-invariant moment map. This way, the action of :math:`\T^n` on :math:`\Arm_3(n,\vec r)` described earlier is hamiltonian. Identifying the Lie algebra of :math:`\T^n` with :math:`\R^n` and :math:`(\R^n)^\ast` with :math:`\R^n` via the usual inner product on :math:`\R^n`, the :math:`\T^n`-invariant moment map is given by

   .. math:: \mu\colon \Arm_3(n,\vec r)\to \R^n, ((x_1,y_1,z_1),\dots,(x_n,y_n,z_n))\mapsto (z_1,             \dotsc,z_n).``

   We then have :math:`\mu(M)= \prod_{i=1}^n\mu_i(M_i)=\prod_{i=1}^n [-r_i,r_i]`.  

:math:`\Arm_3(n;\vec r)` is an example of what is called a *symplectic toric manifold*: 

Symplectic Toric Manifolds
--------------------------

**Definition:**
A *symplectic toric manifold* is a :math:`2n`-dimensional compact connected symplectic manifold :math:`(M,\omega)` together with an effective hamiltonian :math:`\T^n`-action and a choice of a :math:`\T^n`-invariant moment map :math:`\mu`. :math:`\mu(M)` is then called the moment polytope.

Note that the fixed points of the action of :math:`\T^n` on :math:`\Arm_3(n,\vec r)` are exactly the preimages of the vertices :math:`(\pm r_1,\dotsc,\pm r_n)` of :math:`\mu(M)` under :math:`\mu`. This is no coincidence, as the following theorem due to Atiyah :cite:`Atiyah` and Guillemin-Sternberg :cite:`Guillemin-Sternberg` shows:

**Theorem:**
Let :math:`M` be a :math:`2n`-dimensional symplectic toric manifold with :math:`\T^n`-invariant moment map :math:`\mu\colon M\to \mathbb R^n`. We then have:

1. :math:`\mu^{-1}(c)` is connected for each :math:`c\in\mathbb R^n`;
2. :math:`\mu(M)` is the convex hull of the fixed points of the :math:`\T^n`-action.

Action-Angle Coordinates
------------------------

A toric symplectic manifold :math:`(M,\omega)` with :math:`\T^n`-invariant moment map :math:`\mu=(\mu_1,\dotsc,\mu_n)` is a special case of what is called an *integrable system*. The Arnold-Liouville theorem (see for example :cite:`Cannas`, :cite:`Arnold`) then tells us that for :math:`c\in \R^n` a regular value of :math:`\mu`, :math:`\mu^{-1}(c)` is diffeomorphic to :math:`\T^n` and in an open neighborhood :math:`U` of :math:`\mu^{-1}(c)` there are coordinates :math:`(I_1,\dotsc,I_n,\varphi_1,\dotsc,\varphi_n)\colon U\to \tilde U\times \T^n` for :math:`\tilde U\subset \R^n` open in which :math:`\T^n` acts on the last :math:`n` coordinates and :math:`\omega=\sum_{i=1}^n \dif I_i\wedge \dif \varphi_i`.

Letting :math:`P` denote the moment polytope :math:`\mu(M)`, a smooth map :math:`\alpha\colon P\times \T^n\to M` is called a *(global) action-angle parametrization of M* if the restriction of :math:`\alpha` to :math:`\text{int}(P)\times \T^n` is a symplectomorphism onto a full measure subset of :math:`M`. 

**Examples:**

1. The map :math:`[-r,r]\times \Sph^1\to \Sph^2_r`, :math:`(h, \varphi)\mapsto (r\cdot \cos(\varphi), r\cdot\sin(\varphi),h)` is an action-angle parametrization of :math:`\Sph^2_r`.
2. Applying this componentwise to :math:`\Arm_3(\vec r;n)`, we get the action-angle parametrization :math:`\prod_{i=1}^n[-r_i,r_i] \times \T^n\to \Arm_3(\vec r;n)`, which restriction to :math:`\prod_{i=1}^n (-r_i,r_i)\times \T^n` we already described earlier.

It's immediate that we can sample symplectic toric manifolds that admit a global action-angle parametrization the same way as we sample :math:`\Arm_3(\vec r;n)`. 

.. bibliography:: biblio.bib
