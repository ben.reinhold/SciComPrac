Welcome to Random Walks's documentation!
========================================

This is the documentation of a project that was created as part of the module "Praktikum Wissenschaftliches Rechnen" at the University of Goettingen under the advision of Prof. Max Wardetzky and Dr. Jochen Schulz. You can find its GitLab page here_.

.. _here: https://gitlab.gwdg.de/ben.reinhold/SciComPrac

The python module `pwalk` implements and visualizes the random movement of an open polymer arm in 3-space using the "toric symplectic Markov Chain Monte Carlo algorithm" from :cite:`Cantarella-Shonkwiler`. This simulates for example the random movement of an ideal polymer chain.

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   :name: mastertoc

   examples
   pwalk
   mathBG	