﻿# Random Walks
[![Documentation Status](https://readthedocs.org/projects/random-walks-scientific-computing-practical/badge/?version=latest)](https://random-walks-scientific-computing-practical.readthedocs.io/en/latest/?badge=latest)

This project was created as part of the module "Praktikum Wissenschaftliches Rechnen" at the University of Göttingen under the advision of Prof. Max Wardetzky and Dr. Jochen Schulz.

It implements and visualizes an algorithm from a [paper](https://projecteuclid.org/euclid.aoap/1452003247) by Jason Cantarella and Clayton Shonkwiler that uses a symplectic geometric approach to generate the random movement of an open polymer arm in 3-space. This simulates for example the random movement of an ideal polymer chain. With this symplectic geometric approach, constraints on the movement can easily be added in the form of linear inequalities on the "action" coordiantes of the configuration space (see [here](https://random-walks-scientific-computing-practical.readthedocs.io/en/latest/mathBG.html) for details). 

## Table of Contents
- [Installation](#installation)
   - [Windows](#windows)
      - [Mayavi](#mayavi)
      - [Moviepy](#moviepy)
   -  [Linux](#linux)
   - [MacOS](#macos)
      - [Mayavi](#mayavi-1)
      - [Moviepy](#moviepy-1)
   - [First Steps](#first-steps)
- [Documentation](#documentation)
- [Useful Resources](#useful-resources)


## Installation

To install the python packages needed, we assume that `python` and the package manager `conda` and it dependencies are already installed, preferably either by [Miniconda](https://conda.io/miniconda.html) or as part of the [Anaconda](https://www.anaconda.com/download/) distribution.

### Windows


#### Mayavi

For 3D-plotting, we use the the python package `mayavi`. It has many dependencies and should preferably be installed in a seperate enviroment:

```
conda create -n env_name python=3.5 pyqt=4
activate env_name
conda install -c menpo mayavi
```

For further reference, see the [mayavi installation page](https://docs.enthought.com/mayavi/mayavi/installation.html).

#### MoviePy

For animation purposes, we use `MoviePy`. To install this, run

```
pip install MoviePy
conda install requests
```

in the enviroment `env_name` created earlier.


### Linux

The installation on Linux systems is the same as on Windows, you just have to substitute `activate` with `conda activate`. Alternatively, if you want to use `python 3.6`, you can run:

```
conda create -n env_name python=3.6 numpy
conda activate env_name
pip install vtk
pip install mayavi pyqt5 moviepy requests
```

### MacOS

#### Mayavi

For 3D-plotting, we use the the python package `mayavi`. It has many dependencies and should preferably be installed in a seperate enviroment:

```
conda create -n env_name python=3.5 pyqt=4
activate env_name
pip install mayavi
```


For further reference, see the [mayavi installation page](https://docs.enthought.com/mayavi/mayavi/installation.html).

#### MoviePy

For animation purposes, we use `MoviePy`. To install this, run

```
pip install MoviePy
conda install requests
```

in the enviroment `env_name` created earlier.


### First Steps

After completing the installation of `mayavi` and `MoviePy` you might want to check out the [examples](https://random-walks-scientific-computing-practical.readthedocs.io/en/latest/examples.html) page of the documentation. Note that not every python editor might be compatible with the dependencies of `mayavi`. A convenient (and working) choice would be `jupyter`. Just run `conda install jupyter` or `pip install jupyter` in `env_name` to install it and open it by running `jupyter notebook`. For further reference, see [here](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html).

**Note:** If your computer has multiple graphic cards (common for laptops), you  might want to make sure that the best one is prioritized. Error messages regarding the support of `OpenGL` might indicate that this is not currently the case.

## Documentation

The [documentation](https://random-walks-scientific-computing-practical.readthedocs.io/en/latest/#) is hosted on readthedocs.io. It is updated automatically at each commit. It follows the [numpydoc docstring guide](https://numpydoc.readthedocs.io/en/latest/format.html).

### Sphinx

If you want to generate the documentation locally, install `sphinx` and the two extensions `numpydoc` and `sphinxcontrib-bibtex` via

```
pip install sphinx
pip install numpydoc sphinxcontrib-bibtex
```

into the enviroment `env_name` created earlier and and run `make html` in `docs/`. The documentation will then be build in `docs/_build/html/`. If you have a TeX distribution installed on your system, you can run `make latex` instead and compile `docs/_build/latex/RandomWalks.tex` with `pdflatex` to obtain the pdf version of the documentation. 

Look [here](http://www.sphinx-doc.org/en/1.6.1/tutorial.html) for an introduction to sphinx. We use the MathJax extension to render math in html. For math in the documentation, use the rst-native math [directive](http://docutils.sourceforge.net/docs/ref/rst/directives.html#math)/[role](http://docutils.sourceforge.net/docs/ref/rst/roles.html#math). You can add global latex macros in `/docs/conf.py` in the entries `mathjax_config` and `latex_elements`, once for the html documentation and once for the pdf version.


**Note:** Make sure to use a sphinx version of 1.8 or higher as the configuration of MathJax via `conf.py` is not supported in older versions.

## Useful Resources

[reStructuredText (.rst) Cheatsheet](http://docutils.sourceforge.net/docs/user/rst/quickref.html)  
[Markdown (.md) Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)  
[PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)    
[Numpydoc Docstring Guide](https://numpydoc.readthedocs.io/en/latest/format.html)  